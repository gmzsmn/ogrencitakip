﻿namespace OgrenciBasariAnaliziUygulamasi
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.studentSaveForm = new System.Windows.Forms.TabPage();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtStudentSearch = new System.Windows.Forms.TextBox();
            this.lblStudentSearch = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtStudentNu = new System.Windows.Forms.TextBox();
            this.lblStudentNo = new System.Windows.Forms.Label();
            this.txtstudentSurname = new System.Windows.Forms.TextBox();
            this.lblStudentSurname = new System.Windows.Forms.Label();
            this.txtBoxStudentName = new System.Windows.Forms.TextBox();
            this.lblStudentName = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblDers = new System.Windows.Forms.Label();
            this.cmbDers = new System.Windows.Forms.ComboBox();
            this.btnGrafikOlustur = new System.Windows.Forms.Button();
            this.lblDonem = new System.Windows.Forms.Label();
            this.cmbDonem = new System.Windows.Forms.ComboBox();
            this.lblSube = new System.Windows.Forms.Label();
            this.lblSinif = new System.Windows.Forms.Label();
            this.cmbSube = new System.Windows.Forms.ComboBox();
            this.cmbSinif = new System.Windows.Forms.ComboBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabControl1.SuspendLayout();
            this.studentSaveForm.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.studentSaveForm);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(673, 470);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // studentSaveForm
            // 
            this.studentSaveForm.Controls.Add(this.btnDelete);
            this.studentSaveForm.Controls.Add(this.btnSearch);
            this.studentSaveForm.Controls.Add(this.txtStudentSearch);
            this.studentSaveForm.Controls.Add(this.lblStudentSearch);
            this.studentSaveForm.Controls.Add(this.btnSave);
            this.studentSaveForm.Controls.Add(this.txtStudentNu);
            this.studentSaveForm.Controls.Add(this.lblStudentNo);
            this.studentSaveForm.Controls.Add(this.txtstudentSurname);
            this.studentSaveForm.Controls.Add(this.lblStudentSurname);
            this.studentSaveForm.Controls.Add(this.txtBoxStudentName);
            this.studentSaveForm.Controls.Add(this.lblStudentName);
            this.studentSaveForm.Location = new System.Drawing.Point(4, 22);
            this.studentSaveForm.Name = "studentSaveForm";
            this.studentSaveForm.Padding = new System.Windows.Forms.Padding(3);
            this.studentSaveForm.Size = new System.Drawing.Size(665, 444);
            this.studentSaveForm.TabIndex = 0;
            this.studentSaveForm.Text = "Öğrenci Kayıt Formu";
            this.studentSaveForm.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(207, 134);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 23);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "Sil";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(325, 13);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(100, 23);
            this.btnSearch.TabIndex = 9;
            this.btnSearch.Text = "Ara";
            this.btnSearch.UseMnemonic = false;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtStudentSearch
            // 
            this.txtStudentSearch.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtStudentSearch.Location = new System.Drawing.Point(101, 16);
            this.txtStudentSearch.Name = "txtStudentSearch";
            this.txtStudentSearch.Size = new System.Drawing.Size(218, 20);
            this.txtStudentSearch.TabIndex = 8;
            this.txtStudentSearch.Text = "Öğrenci No Giriniz";
            this.txtStudentSearch.TextChanged += new System.EventHandler(this.txtStudentSearch_TextChanged);
            this.txtStudentSearch.Enter += new System.EventHandler(this.txtStudentSearch_Enter);
            this.txtStudentSearch.Leave += new System.EventHandler(this.txtStudentSearch_Leave);
            // 
            // lblStudentSearch
            // 
            this.lblStudentSearch.AutoSize = true;
            this.lblStudentSearch.Location = new System.Drawing.Point(10, 21);
            this.lblStudentSearch.Name = "lblStudentSearch";
            this.lblStudentSearch.Size = new System.Drawing.Size(69, 13);
            this.lblStudentSearch.TabIndex = 7;
            this.lblStudentSearch.Text = "Öğrenci Ara :";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(101, 134);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Kaydet";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtStudentNu
            // 
            this.txtStudentNu.Location = new System.Drawing.Point(101, 108);
            this.txtStudentNu.Name = "txtStudentNu";
            this.txtStudentNu.Size = new System.Drawing.Size(100, 20);
            this.txtStudentNu.TabIndex = 5;
            // 
            // lblStudentNo
            // 
            this.lblStudentNo.AutoSize = true;
            this.lblStudentNo.Location = new System.Drawing.Point(10, 115);
            this.lblStudentNo.Name = "lblStudentNo";
            this.lblStudentNo.Size = new System.Drawing.Size(67, 13);
            this.lblStudentNo.TabIndex = 4;
            this.lblStudentNo.Text = "Öğrenci No :";
            // 
            // txtstudentSurname
            // 
            this.txtstudentSurname.Location = new System.Drawing.Point(101, 82);
            this.txtstudentSurname.Name = "txtstudentSurname";
            this.txtstudentSurname.Size = new System.Drawing.Size(100, 20);
            this.txtstudentSurname.TabIndex = 3;
            // 
            // lblStudentSurname
            // 
            this.lblStudentSurname.AutoSize = true;
            this.lblStudentSurname.Location = new System.Drawing.Point(10, 87);
            this.lblStudentSurname.Name = "lblStudentSurname";
            this.lblStudentSurname.Size = new System.Drawing.Size(85, 13);
            this.lblStudentSurname.TabIndex = 2;
            this.lblStudentSurname.Text = "Öğrenci Soyadı :";
            // 
            // txtBoxStudentName
            // 
            this.txtBoxStudentName.Location = new System.Drawing.Point(101, 56);
            this.txtBoxStudentName.Name = "txtBoxStudentName";
            this.txtBoxStudentName.Size = new System.Drawing.Size(100, 20);
            this.txtBoxStudentName.TabIndex = 1;
            // 
            // lblStudentName
            // 
            this.lblStudentName.AutoSize = true;
            this.lblStudentName.Location = new System.Drawing.Point(10, 61);
            this.lblStudentName.Name = "lblStudentName";
            this.lblStudentName.Size = new System.Drawing.Size(68, 13);
            this.lblStudentName.TabIndex = 0;
            this.lblStudentName.Text = "Öğrenci Adı :";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lblDers);
            this.tabPage2.Controls.Add(this.cmbDers);
            this.tabPage2.Controls.Add(this.btnGrafikOlustur);
            this.tabPage2.Controls.Add(this.lblDonem);
            this.tabPage2.Controls.Add(this.cmbDonem);
            this.tabPage2.Controls.Add(this.lblSube);
            this.tabPage2.Controls.Add(this.lblSinif);
            this.tabPage2.Controls.Add(this.cmbSube);
            this.tabPage2.Controls.Add(this.cmbSinif);
            this.tabPage2.Controls.Add(this.chart1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(665, 444);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Başarı Grafiği";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblDers
            // 
            this.lblDers.AutoSize = true;
            this.lblDers.Location = new System.Drawing.Point(291, 29);
            this.lblDers.Name = "lblDers";
            this.lblDers.Size = new System.Drawing.Size(47, 13);
            this.lblDers.TabIndex = 9;
            this.lblDers.Text = "Ders     :";
            // 
            // cmbDers
            // 
            this.cmbDers.FormattingEnabled = true;
            this.cmbDers.Location = new System.Drawing.Point(380, 24);
            this.cmbDers.Name = "cmbDers";
            this.cmbDers.Size = new System.Drawing.Size(121, 21);
            this.cmbDers.TabIndex = 8;
            // 
            // btnGrafikOlustur
            // 
            this.btnGrafikOlustur.Location = new System.Drawing.Point(210, 101);
            this.btnGrafikOlustur.Name = "btnGrafikOlustur";
            this.btnGrafikOlustur.Size = new System.Drawing.Size(128, 23);
            this.btnGrafikOlustur.TabIndex = 7;
            this.btnGrafikOlustur.Text = "Grafik Oluştur";
            this.btnGrafikOlustur.UseVisualStyleBackColor = true;
            this.btnGrafikOlustur.Click += new System.EventHandler(this.btnGrafikOlustur_Click);
            // 
            // lblDonem
            // 
            this.lblDonem.AutoSize = true;
            this.lblDonem.Location = new System.Drawing.Point(291, 61);
            this.lblDonem.Name = "lblDonem";
            this.lblDonem.Size = new System.Drawing.Size(47, 13);
            this.lblDonem.TabIndex = 6;
            this.lblDonem.Text = "Dönem :";
            // 
            // cmbDonem
            // 
            this.cmbDonem.FormattingEnabled = true;
            this.cmbDonem.Location = new System.Drawing.Point(380, 56);
            this.cmbDonem.Name = "cmbDonem";
            this.cmbDonem.Size = new System.Drawing.Size(121, 21);
            this.cmbDonem.TabIndex = 5;
            // 
            // lblSube
            // 
            this.lblSube.AutoSize = true;
            this.lblSube.Location = new System.Drawing.Point(24, 64);
            this.lblSube.Name = "lblSube";
            this.lblSube.Size = new System.Drawing.Size(38, 13);
            this.lblSube.TabIndex = 4;
            this.lblSube.Text = "Şube :";
            // 
            // lblSinif
            // 
            this.lblSinif.AutoSize = true;
            this.lblSinif.Location = new System.Drawing.Point(24, 32);
            this.lblSinif.Name = "lblSinif";
            this.lblSinif.Size = new System.Drawing.Size(36, 13);
            this.lblSinif.TabIndex = 3;
            this.lblSinif.Text = "Sınıf : ";
            // 
            // cmbSube
            // 
            this.cmbSube.FormattingEnabled = true;
            this.cmbSube.Location = new System.Drawing.Point(98, 61);
            this.cmbSube.Name = "cmbSube";
            this.cmbSube.Size = new System.Drawing.Size(121, 21);
            this.cmbSube.TabIndex = 2;
            // 
            // cmbSinif
            // 
            this.cmbSinif.FormattingEnabled = true;
            this.cmbSinif.Location = new System.Drawing.Point(98, 29);
            this.cmbSinif.Name = "cmbSinif";
            this.cmbSinif.Size = new System.Drawing.Size(121, 21);
            this.cmbSinif.TabIndex = 1;
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(0, 121);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(583, 300);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 485);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "asd";
            this.tabControl1.ResumeLayout(false);
            this.studentSaveForm.ResumeLayout(false);
            this.studentSaveForm.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage studentSaveForm;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lblStudentName;
        private System.Windows.Forms.TextBox txtBoxStudentName;
        private System.Windows.Forms.TextBox txtStudentNu;
        private System.Windows.Forms.Label lblStudentNo;
        private System.Windows.Forms.TextBox txtstudentSurname;
        private System.Windows.Forms.Label lblStudentSurname;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtStudentSearch;
        private System.Windows.Forms.Label lblStudentSearch;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblDonem;
        private System.Windows.Forms.ComboBox cmbDonem;
        private System.Windows.Forms.Label lblSube;
        private System.Windows.Forms.Label lblSinif;
        private System.Windows.Forms.ComboBox cmbSube;
        private System.Windows.Forms.ComboBox cmbSinif;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button btnGrafikOlustur;
        private System.Windows.Forms.Label lblDers;
        private System.Windows.Forms.ComboBox cmbDers;

    }
}

