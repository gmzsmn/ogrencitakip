﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OgrenciBasariAnaliziUygulamasi
{
    class Branch
    {
        public static Dictionary<string, string> GetAllBranches()
        {
            var dictionary = new Dictionary<string, string>();
            dictionary.Add("A", "A Grubu");
            dictionary.Add("B", "B Grubu");
            dictionary.Add("C", "C Grubu");
            dictionary.Add("D", "D Grubu");

            return dictionary;
        }
    }
}
