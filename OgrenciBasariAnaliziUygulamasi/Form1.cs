﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OgrenciBasariAnaliziUygulamasi
{
    public partial class Form1 : Form
    {
        StudentTrackingEntities context = new StudentTrackingEntities();
        studentInfo std;
        public Form1()
        {
            InitializeComponent();
        }

        private void txtStudentSearch_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtStudentSearch_Enter(object sender, EventArgs e)
        {
            txtStudentSearch.Text = null;
            txtStudentSearch.ForeColor = Color.FromKnownColor(KnownColor.Desktop);
        }

        private void txtStudentSearch_Leave(object sender, EventArgs e)
        {

            if (String.IsNullOrWhiteSpace(txtStudentSearch.Text))
            {
                txtStudentSearch.Text = "Öğrenci No Giriniz";
                txtStudentSearch.ForeColor = Color.FromKnownColor(KnownColor.InactiveCaption);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
          //  using (context)
         //   {
                 std = context.studentInfo
                .Where(x => x.studentNo == txtStudentSearch.Text)
                .FirstOrDefault();
                if (std == null)
                {
                    MessageBox.Show("Öğrenci bulunamadı.");
                    return;
                }
           
                    txtBoxStudentName.Text = std.name;
                    txtstudentSurname.Text = std.surname;
                    txtStudentNu.Text = std.studentNo;
                    btnDelete.Enabled = true;
                
         //   }   
                     
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            string message = "Kayıt Güncellendi!";
            if (std == null) { 
                std = new studentInfo();
                context.studentInfo.Add(std);
                message = "Yeni Kayıt Eklendi!";

            }

            std.name = txtBoxStudentName.Text;
            std.surname = txtstudentSurname.Text;
            std.studentNo = txtStudentNu.Text;
            context.SaveChanges();
            MessageBox.Show(message);
            Clear();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            
            if (std == null)
            {
                MessageBox.Show("kayıt bulunamadı");
                return;

            }


            context.studentInfo.Remove(std);
            context.SaveChanges();
            MessageBox.Show("Kayıt Silindi");

            Clear();

        }


        private void Clear()
        {
            txtBoxStudentName.Text = null;
            txtstudentSurname.Text = null;
            txtStudentNu.Text = null;
            std = null;
        }

        //private void tabPage2_Click(object sender, EventArgs e)
        //{
        //    Fillcombobox();
        //}
        void Fillcombobox()
        {
            

            var allLectures = Lecture.GetAllLectures();
            cmbDers.DataSource = new  BindingSource(allLectures, null);
            cmbDers.DisplayMember = "Value";
            cmbDers.ValueMember = "Key";


            var allBranches = Branch.GetAllBranches();
            cmbSube.DataSource = new BindingSource(allBranches, null);
            cmbSube.DisplayMember = "Value";
            cmbSube.ValueMember = "Key";


        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            Fillcombobox();
        }

        private void btnGrafikOlustur_Click(object sender, EventArgs e)
        {
            context.StudentGrades.Where(s => s.lectureName == cmbDers.SelectedValue).FirstOrDefault();
        }

   
    }

   /*    using(var context= new StudentTrackingEntities())
            {
                var stdinfo = new studentInfo();
                stdinfo.name = "ayşe";
                stdinfo.surname = "durmaz";
                stdinfo.studentNo = "100";
                context.studentInfo.Add(stdinfo);
                context.SaveChanges();
            }*/
}
