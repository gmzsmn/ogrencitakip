﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OgrenciBasariAnaliziUygulamasi
{
    public class Lecture
    {
        public static Dictionary<string, string> GetAllLectures()
        {
            var dictionary = new Dictionary<string, string>();
            dictionary.Add("MAT", "Matematik");
            dictionary.Add("TUR", "Türkçe");
            dictionary.Add("MUZ", "Müzik");
            dictionary.Add("COG", "Coğrafya");

            return dictionary;
        }

        
    }
}
